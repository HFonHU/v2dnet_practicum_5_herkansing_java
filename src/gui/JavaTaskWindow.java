package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import org.tempuri.*;

public class JavaTaskWindow {

	private JFrame frame;
	private JTextField modifyTextBox;
	private JTextField overViewTextBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JavaTaskWindow window = new JavaTaskWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JavaTaskWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 366);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Overview", null, panel, null);
		panel.setLayout(null);
		
		JList overviewTaskList = new JList();
		overviewTaskList.setBounds(10, 64, 185, 166);
		panel.add(overviewTaskList);
		
		JLabel overviewHeaderLabel = new JLabel("Welcome to project ");
		overviewHeaderLabel.setBounds(135, 11, 162, 14);
		panel.add(overviewHeaderLabel);
		
		JLabel overviewHoursWorked = new JLabel("Hours worked:");
		overviewHoursWorked.setBounds(253, 65, 86, 14);
		panel.add(overviewHoursWorked);
		
		JButton overviewHoursSave = new JButton("Save");
		overviewHoursSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		overviewHoursSave.setBounds(166, 266, 89, 23);
		panel.add(overviewHoursSave);
		
		JLabel OverviewAddHoursLabel = new JLabel("");
		OverviewAddHoursLabel.setBounds(10, 241, 409, 14);
		panel.add(OverviewAddHoursLabel);
		
		overViewTextBox = new JTextField();
		overViewTextBox.setBounds(253, 90, 86, 20);
		panel.add(overViewTextBox);
		overViewTextBox.setColumns(10);
		
		JLabel overviewAvailableTasksLabel = new JLabel("Available tasks: ");
		overviewAvailableTasksLabel.setBounds(10, 39, 86, 14);
		panel.add(overviewAvailableTasksLabel);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Modify Task Hours", null, panel_1, null);
		panel_1.setLayout(null);
		
		JList ModifyTaskList = new JList();
		ModifyTaskList.setBounds(10, 31, 182, 190);
		panel_1.add(ModifyTaskList);
		
		JButton modifyDeleteButton = new JButton("Delete Hours");
		modifyDeleteButton.setBounds(10, 266, 108, 23);
		panel_1.add(modifyDeleteButton);
		
		JButton ModifyHoursButton = new JButton("Edit Hours");
		ModifyHoursButton.setBounds(242, 266, 89, 23);
		panel_1.add(ModifyHoursButton);
		
		JLabel ModifyRegisteredTasksLabel = new JLabel("Today's tasks:");
		ModifyRegisteredTasksLabel.setBounds(10, 11, 409, 14);
		panel_1.add(ModifyRegisteredTasksLabel);
		
		JLabel modifyTaskTimeLabel = new JLabel("Registration time: ");
		modifyTaskTimeLabel.setBounds(245, 32, 108, 14);
		panel_1.add(modifyTaskTimeLabel);
		
		JLabel ModifyCurrentHoursLabel = new JLabel("Current hours: ");
		ModifyCurrentHoursLabel.setBounds(245, 57, 89, 14);
		panel_1.add(ModifyCurrentHoursLabel);
		
		modifyTextBox = new JTextField();
		modifyTextBox.setBounds(242, 201, 86, 20);
		panel_1.add(modifyTextBox);
		modifyTextBox.setColumns(10);
		
		JLabel modifyNewHoursLabel = new JLabel("New number of registered hours: ");
		modifyNewHoursLabel.setBounds(242, 176, 177, 14);
		panel_1.add(modifyNewHoursLabel);
		
		JLabel modifyMessageLabel = new JLabel("");
		modifyMessageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		modifyMessageLabel.setBounds(10, 232, 409, 14);
		panel_1.add(modifyMessageLabel);
	}
}
