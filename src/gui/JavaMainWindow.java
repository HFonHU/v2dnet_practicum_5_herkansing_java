package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import org.tempuri.*;

public class JavaMainWindow {

	private JFrame frame;
	private JTextField registerUsernameBox;
	private JPasswordField logInPasswordField;
	private JTextField logInUserNameTextBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JavaMainWindow window = new JavaMainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JavaMainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 262);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Register", null, panel, null);
		panel.setLayout(null);
		
		JLabel registrationHeaderLabel = new JLabel("Please register an account");
		registrationHeaderLabel.setHorizontalAlignment(SwingConstants.CENTER);
		registrationHeaderLabel.setBounds(56, 11, 288, 14);
		panel.add(registrationHeaderLabel);
		
		JLabel registerUserNameLabel = new JLabel("Username");
		registerUserNameLabel.setBounds(41, 64, 77, 14);
		panel.add(registerUserNameLabel);
		
		JButton registerButton = new JButton("Register");
		registerButton.setBounds(154, 200, 89, 23);
		panel.add(registerButton);
		
		JComboBox registerProjectNameComboBox = new JComboBox();
		registerProjectNameComboBox.setBounds(243, 133, 156, 20);
		panel.add(registerProjectNameComboBox);
		
		JLabel registerMessageLabel = new JLabel("");
		registerMessageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		registerMessageLabel.setBounds(10, 175, 409, 14);
		panel.add(registerMessageLabel);
		
		registerUsernameBox = new JTextField();
		registerUsernameBox.setBounds(243, 61, 156, 20);
		panel.add(registerUsernameBox);
		registerUsernameBox.setColumns(10);
		
		JLabel registerProjectLabel = new JLabel("Project name");
		registerProjectLabel.setBounds(41, 136, 89, 14);
		panel.add(registerProjectLabel);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Log in", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel loginHeaderLabel = new JLabel("Please log in");
		loginHeaderLabel.setHorizontalAlignment(SwingConstants.CENTER);
		loginHeaderLabel.setBounds(151, 11, 109, 14);
		panel_1.add(loginHeaderLabel);
		
		JLabel logInMessageLabel = new JLabel("");
		logInMessageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		logInMessageLabel.setBounds(10, 174, 409, 14);
		panel_1.add(logInMessageLabel);
		
		logInPasswordField = new JPasswordField();
		logInPasswordField.setBounds(283, 129, 109, 20);
		panel_1.add(logInPasswordField);
		
		logInUserNameTextBox = new JTextField();
		logInUserNameTextBox.setBounds(283, 67, 109, 20);
		panel_1.add(logInUserNameTextBox);
		logInUserNameTextBox.setColumns(10);
		
		JLabel logInUsernameLabel = new JLabel("Username");
		logInUsernameLabel.setBounds(47, 70, 62, 14);
		panel_1.add(logInUsernameLabel);
		
		JLabel logInPasswordBox = new JLabel("Password");
		logInPasswordBox.setBounds(47, 135, 46, 14);
		panel_1.add(logInPasswordBox);
		
		JButton logInButton = new JButton("Log in");
		logInButton.setBounds(171, 200, 89, 23);
		panel_1.add(logInButton);
	}
}
