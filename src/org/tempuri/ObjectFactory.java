
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetRegisteredTaskDetailsResponseGetRegisteredTaskDetailsResult_QNAME = new QName("http://tempuri.org/", "GetRegisteredTaskDetailsResult");
    private final static QName _LogInUserPassword_QNAME = new QName("http://tempuri.org/", "userPassword");
    private final static QName _LogInUserName_QNAME = new QName("http://tempuri.org/", "userName");
    private final static QName _GetProjectNameResponseGetProjectNameResult_QNAME = new QName("http://tempuri.org/", "GetProjectNameResult");
    private final static QName _RegisterUserResponseRegisterUserResult_QNAME = new QName("http://tempuri.org/", "RegisterUserResult");
    private final static QName _AddTaskHoursTaskName_QNAME = new QName("http://tempuri.org/", "taskName");
    private final static QName _AddTaskHoursUsername_QNAME = new QName("http://tempuri.org/", "username");
    private final static QName _RegisterUserProjectName_QNAME = new QName("http://tempuri.org/", "projectName");
    private final static QName _GetAllAvailableProjectsResponseGetAllAvailableProjectsResult_QNAME = new QName("http://tempuri.org/", "GetAllAvailableProjectsResult");
    private final static QName _GetAllTasksResponseGetAllTasksResult_QNAME = new QName("http://tempuri.org/", "GetAllTasksResult");
    private final static QName _GetAllTasksWorkedOnResponseGetAllTasksWorkedOnResult_QNAME = new QName("http://tempuri.org/", "GetAllTasksWorkedOnResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IsUserExistsResponse }
     * 
     */
    public IsUserExistsResponse createIsUserExistsResponse() {
        return new IsUserExistsResponse();
    }

    /**
     * Create an instance of {@link GetRegisteredTaskDetailsResponse }
     * 
     */
    public GetRegisteredTaskDetailsResponse createGetRegisteredTaskDetailsResponse() {
        return new GetRegisteredTaskDetailsResponse();
    }

    /**
     * Create an instance of {@link GetAllTasksWorkedOnResponse }
     * 
     */
    public GetAllTasksWorkedOnResponse createGetAllTasksWorkedOnResponse() {
        return new GetAllTasksWorkedOnResponse();
    }

    /**
     * Create an instance of {@link EditRegisteredTaskHours }
     * 
     */
    public EditRegisteredTaskHours createEditRegisteredTaskHours() {
        return new EditRegisteredTaskHours();
    }

    /**
     * Create an instance of {@link DeleteRegisteredTaskHours }
     * 
     */
    public DeleteRegisteredTaskHours createDeleteRegisteredTaskHours() {
        return new DeleteRegisteredTaskHours();
    }

    /**
     * Create an instance of {@link LogIn }
     * 
     */
    public LogIn createLogIn() {
        return new LogIn();
    }

    /**
     * Create an instance of {@link GetAllTasks }
     * 
     */
    public GetAllTasks createGetAllTasks() {
        return new GetAllTasks();
    }

    /**
     * Create an instance of {@link RegisterUser }
     * 
     */
    public RegisterUser createRegisterUser() {
        return new RegisterUser();
    }

    /**
     * Create an instance of {@link LogInResponse }
     * 
     */
    public LogInResponse createLogInResponse() {
        return new LogInResponse();
    }

    /**
     * Create an instance of {@link GetAllTasksWorkedOn }
     * 
     */
    public GetAllTasksWorkedOn createGetAllTasksWorkedOn() {
        return new GetAllTasksWorkedOn();
    }

    /**
     * Create an instance of {@link DeleteRegisteredTaskHoursResponse }
     * 
     */
    public DeleteRegisteredTaskHoursResponse createDeleteRegisteredTaskHoursResponse() {
        return new DeleteRegisteredTaskHoursResponse();
    }

    /**
     * Create an instance of {@link GetRegisteredTaskDetails }
     * 
     */
    public GetRegisteredTaskDetails createGetRegisteredTaskDetails() {
        return new GetRegisteredTaskDetails();
    }

    /**
     * Create an instance of {@link GetProjectNameResponse }
     * 
     */
    public GetProjectNameResponse createGetProjectNameResponse() {
        return new GetProjectNameResponse();
    }

    /**
     * Create an instance of {@link GetAllAvailableProjectsResponse }
     * 
     */
    public GetAllAvailableProjectsResponse createGetAllAvailableProjectsResponse() {
        return new GetAllAvailableProjectsResponse();
    }

    /**
     * Create an instance of {@link RegisterUserResponse }
     * 
     */
    public RegisterUserResponse createRegisterUserResponse() {
        return new RegisterUserResponse();
    }

    /**
     * Create an instance of {@link GetAllAvailableProjects }
     * 
     */
    public GetAllAvailableProjects createGetAllAvailableProjects() {
        return new GetAllAvailableProjects();
    }

    /**
     * Create an instance of {@link GetAllTasksResponse }
     * 
     */
    public GetAllTasksResponse createGetAllTasksResponse() {
        return new GetAllTasksResponse();
    }

    /**
     * Create an instance of {@link GetProjectName }
     * 
     */
    public GetProjectName createGetProjectName() {
        return new GetProjectName();
    }

    /**
     * Create an instance of {@link AddTaskHoursResponse }
     * 
     */
    public AddTaskHoursResponse createAddTaskHoursResponse() {
        return new AddTaskHoursResponse();
    }

    /**
     * Create an instance of {@link EditRegisteredTaskHoursResponse }
     * 
     */
    public EditRegisteredTaskHoursResponse createEditRegisteredTaskHoursResponse() {
        return new EditRegisteredTaskHoursResponse();
    }

    /**
     * Create an instance of {@link AddTaskHours }
     * 
     */
    public AddTaskHours createAddTaskHours() {
        return new AddTaskHours();
    }

    /**
     * Create an instance of {@link IsUserExists }
     * 
     */
    public IsUserExists createIsUserExists() {
        return new IsUserExists();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetRegisteredTaskDetailsResult", scope = GetRegisteredTaskDetailsResponse.class)
    public JAXBElement<String> createGetRegisteredTaskDetailsResponseGetRegisteredTaskDetailsResult(String value) {
        return new JAXBElement<String>(_GetRegisteredTaskDetailsResponseGetRegisteredTaskDetailsResult_QNAME, String.class, GetRegisteredTaskDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "userPassword", scope = LogIn.class)
    public JAXBElement<String> createLogInUserPassword(String value) {
        return new JAXBElement<String>(_LogInUserPassword_QNAME, String.class, LogIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "userName", scope = LogIn.class)
    public JAXBElement<String> createLogInUserName(String value) {
        return new JAXBElement<String>(_LogInUserName_QNAME, String.class, LogIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetProjectNameResult", scope = GetProjectNameResponse.class)
    public JAXBElement<String> createGetProjectNameResponseGetProjectNameResult(String value) {
        return new JAXBElement<String>(_GetProjectNameResponseGetProjectNameResult_QNAME, String.class, GetProjectNameResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "userName", scope = IsUserExists.class)
    public JAXBElement<String> createIsUserExistsUserName(String value) {
        return new JAXBElement<String>(_LogInUserName_QNAME, String.class, IsUserExists.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "RegisterUserResult", scope = RegisterUserResponse.class)
    public JAXBElement<String> createRegisterUserResponseRegisterUserResult(String value) {
        return new JAXBElement<String>(_RegisterUserResponseRegisterUserResult_QNAME, String.class, RegisterUserResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "taskName", scope = AddTaskHours.class)
    public JAXBElement<String> createAddTaskHoursTaskName(String value) {
        return new JAXBElement<String>(_AddTaskHoursTaskName_QNAME, String.class, AddTaskHours.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "username", scope = AddTaskHours.class)
    public JAXBElement<String> createAddTaskHoursUsername(String value) {
        return new JAXBElement<String>(_AddTaskHoursUsername_QNAME, String.class, AddTaskHours.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "username", scope = GetAllTasks.class)
    public JAXBElement<String> createGetAllTasksUsername(String value) {
        return new JAXBElement<String>(_AddTaskHoursUsername_QNAME, String.class, GetAllTasks.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "username", scope = GetAllTasksWorkedOn.class)
    public JAXBElement<String> createGetAllTasksWorkedOnUsername(String value) {
        return new JAXBElement<String>(_AddTaskHoursUsername_QNAME, String.class, GetAllTasksWorkedOn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "username", scope = RegisterUser.class)
    public JAXBElement<String> createRegisterUserUsername(String value) {
        return new JAXBElement<String>(_AddTaskHoursUsername_QNAME, String.class, RegisterUser.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "projectName", scope = RegisterUser.class)
    public JAXBElement<String> createRegisterUserProjectName(String value) {
        return new JAXBElement<String>(_RegisterUserProjectName_QNAME, String.class, RegisterUser.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetAllAvailableProjectsResult", scope = GetAllAvailableProjectsResponse.class)
    public JAXBElement<ArrayOfstring> createGetAllAvailableProjectsResponseGetAllAvailableProjectsResult(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetAllAvailableProjectsResponseGetAllAvailableProjectsResult_QNAME, ArrayOfstring.class, GetAllAvailableProjectsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetAllTasksResult", scope = GetAllTasksResponse.class)
    public JAXBElement<ArrayOfstring> createGetAllTasksResponseGetAllTasksResult(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetAllTasksResponseGetAllTasksResult_QNAME, ArrayOfstring.class, GetAllTasksResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "username", scope = GetProjectName.class)
    public JAXBElement<String> createGetProjectNameUsername(String value) {
        return new JAXBElement<String>(_AddTaskHoursUsername_QNAME, String.class, GetProjectName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetAllTasksWorkedOnResult", scope = GetAllTasksWorkedOnResponse.class)
    public JAXBElement<ArrayOfstring> createGetAllTasksWorkedOnResponseGetAllTasksWorkedOnResult(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetAllTasksWorkedOnResponseGetAllTasksWorkedOnResult_QNAME, ArrayOfstring.class, GetAllTasksWorkedOnResponse.class, value);
    }

}
