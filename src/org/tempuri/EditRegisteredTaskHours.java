
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taskRegistrationId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="newTaskHours" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taskRegistrationId",
    "newTaskHours"
})
@XmlRootElement(name = "EditRegisteredTaskHours")
public class EditRegisteredTaskHours {

    protected Integer taskRegistrationId;
    protected Integer newTaskHours;

    /**
     * Gets the value of the taskRegistrationId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaskRegistrationId() {
        return taskRegistrationId;
    }

    /**
     * Sets the value of the taskRegistrationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaskRegistrationId(Integer value) {
        this.taskRegistrationId = value;
    }

    /**
     * Gets the value of the newTaskHours property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNewTaskHours() {
        return newTaskHours;
    }

    /**
     * Sets the value of the newTaskHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNewTaskHours(Integer value) {
        this.newTaskHours = value;
    }

}
